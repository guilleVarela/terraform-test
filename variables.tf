variable "subnet_id_ipfs1" {
    description = "the ipfs1 intance subnet ID "
    type = string
}

variable "subnet_id_ipfs2" {
    description = "the ipfs2 intance subnet ID "
    type = string
}

variable "subnet_id_ipfs3" {
    description = "the ipfs3 intance subnet ID "
    type = string
}

variable "availability_zone_1" {
    description = "the ipfs1 intance availability zone"
    type = string
}

variable "availability_zone_2" {
    description = "the ipfs2 intance availability zone"
    type = string
}

variable "availability_zone_3" {
    description = "the ipfs3 intance availability zone"
    type = string
}