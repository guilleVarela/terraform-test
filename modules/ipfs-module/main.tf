resource "aws_instance" "ipfs" {

  ami                         = var.ami_id
  associate_public_ip_address = false
  instance_type               = var.instance_type
  key_name                    = var.key_name
  availability_zone           = var.availability_zone_ipfs
  vpc_security_group_ids      = aws_security_group.my-security-group.id
  subnet_id                   = var.subnet_id_ipfs
  count                       = var.ec2_count 
}


resource "aws_ebs_volume" "ipfs" {
  availability_zone = var.availability_zone_ipfs
  size              = var.ebs_size
  count             = var.ebs_count

}


resource "aws_volume_attachment" "ipfs-attachement" {
  device_name = var.device_name_ipfs
  volume_id   = aws_ebs_volume.ipfs[count.index].id
  instance_id = aws_instance.ipfs[count.index].id
  count       = var.attachment_count
}


resource "aws_security_group" "my-security-group" {
    name        = var.sg_name
    description = "allow inbound traffic to ports 4001 8080 22"
    ingress {
        from_port = 4001
        to_port = 4001
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    ingress {
        from_port = 8080
        to_port = 8080
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    ingress {
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
}
# To reconciling the created manually sg and the terraform one, it is necessary to run the terraform import command