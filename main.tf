provider "aws" {
  region  = "eu-west-1"
  profile = var.aws_profile
  assume_role {
    role_arn = "arn:aws:iam::blabla:role/adminrole"
  }
}

terraform {
    backend "s3" {
        bucket          = "terraform_bucket_state"
        key             = "terraform"
        region          = "eu-west-1"
        dynamodb_table  = "terraform-locks"
    }
}

module "ipfs" {
    source = "./modules/ipfs-module"

    ipfs1_subnet_id = var.subnet_id_ipfs1
    ipfs1_availability_zone = var.availability_zone_1

    ipfs2_subnet_id = var.subnet_id_ipfs2
    ipfs2_availability_zone = var.availability_zone_2

    ipfs3_subnet_id = var.subnet_id_ipfs3
    ipfs3_availability_zone = var.availability_zone_3
}