variable "ami_id" {
    description = "instance AMI ID"
    type = string
    default = "ami-abcd1234"
}

variable "instance_type" {
    description = "EC2 instance type"
    type = string
    default = "t2.medium"
}

variable "key_name" {
    description = "ssh key name"
    type = string
    default = "ipfs-keypair-name"
}

variable "subnet_id_ipfs" {
    description = "the ipfs1 intance subnet ID "
    type = string
    default = "private-subnetid-1"
}

variable "availability_zone_ipfs" {
    description = "the ipfs1 intance availability zone"
    type = string
    default = "eu-west-1a"
}

variable "ebs_size" {
    description = "the EBS volume size"
    type = number
    default = 100
}

variable "sg_name" {
    description = "the security group name"
    type = string
    default = "sg-ipfs-arn"
}

variable "ec2_count" {
    description = "the number of ec2 instances of the module"
    type = number
    default = 3
}

variable "ebs_count" {
    description = "the number of ebs volumens of the module"
    type = number
    default = 3
}

variable "attachment_count" {
    description = "the number of attachments of the module"
    type = number
    default = 3
}

variable "var.device_name_ipfs" {
    type = string
    default = "/dev/sdh"
}

